import { fetchData } from '../utils/fetchData';

export const setCommodityList = () => dispatch => {
  fetchData('/commodities')
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: 'SET_COMMODITY_LIST',
        commodityList: res
      });
    });
};