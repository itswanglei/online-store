import React, { Component } from 'react';
import './Commodity.less';
import Button from '../shared/Button';
import { fetchData } from '../../utils/fetchData';

class Commodity extends Component {
  constructor(props) {
    super(props);
    this.addCommodityToOrder = this.addCommodityToOrder.bind(this);
  }

  addCommodityToOrder(name) {
    const headers = {
      'Content-Type': 'application/json;charset=UTF-8'
    };
    const data = {
      commodityName: name
    };
    fetchData('/orders', 'POST', headers, data);
  }

  render() {
    const {name, price, unit, imageUrl} = this.props.commodity || [];
    return (
      <div className="commodity">
        <img className="image" src={imageUrl} alt={imageUrl}/>
        <p className="name">{name}</p>
        <p>{`Unit Price：${price} RMB / ${unit}`}</p>
        <div className="button-wrapper">
          <Button
            buttonText="+"
            shape="circle-btn"
            onClick={() => this.addCommodityToOrder(name)}
          />
        </div>
      </div>
    )
  }
}

export default Commodity;