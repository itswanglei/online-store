import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Nav from '../shared/Nav';
import Commodity from './Commodity';
import './Shop.less';
import { setCommodityList } from '../../actions/setCommodityListAction';

class Shop extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.setCommodityList();
  }

  render() {
    return (
      <div className="shop">
        <Nav />
        <ul className="commodity-list">
          {this.props.commodityList.map((commodity, i) => (
            <li key={i} className="commodity-wrapper">
              <Commodity commodity={commodity}/>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({ commodityList: state.shop.commodityList });

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setCommodityList }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shop);