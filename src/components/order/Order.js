import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Nav from '../shared/Nav';
import OrderDetails from './OrderDetails';
import './Order.less';
import { setOrderList } from '../../actions/setOrderListAction';
import { fetchData } from '../../utils/fetchData';

class Order extends Component {
  constructor(props) {
    super(props);
    this.onDelete = this.onDelete.bind(this);
  }

  componentDidMount() {
    this.props.setOrderList();
  }

  onDelete(productName) {
    fetchData(`/orders/${productName}`, 'DELETE')
      .then(() => {
        this.props.setOrderList();
      });
  }

  render() {
    return (
      <div className="order">
        <Nav/>
        <div className="table-wrapper">
          <table>
            <thead>
              <tr>
                <th>名字</th>
                <th>单价 / 元</th>
                <th>数量</th>
                <th>单位</th>
                <th>操作</th>
              </tr>
            </thead>
            <tbody>
              {this.props.orderList.map((product, i) => (
                <OrderDetails key={i} product={product} onDelete={() => this.onDelete(product.name)}/>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ orderList: state.order.orderList });

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setOrderList }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Order);