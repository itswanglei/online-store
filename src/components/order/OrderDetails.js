import React, { Component } from 'react';
import Button from '../shared/Button';

class OrderDetails extends Component {
  constructor(props) {
    super(props);
    this.onDelete = this.onDelete.bind(this);
  }

  onDelete() {
    this.props.onDelete();
  }

  render() {
    const {name, price, count, unit} = this.props.product || [];
    return (
      <tr>
        <td>{name}</td>
        <td>{price}</td>
        <td>{count}</td>
        <td>{unit}</td>
        <td>
          <Button
            buttonText="删除"
            textColor="red-text"
            onClick={this.onDelete}
          />
        </td>
      </tr>
    );
  }
}

export default OrderDetails;