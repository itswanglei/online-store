import React, { Component } from 'react';
import Nav from '../shared/Nav';
import './Creator.less';
import Button from '../shared/Button';
import { fetchData } from '../../utils/fetchData';

class Creator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      price: '',
      unit: '',
      imageUrl: ''
    };

    this.createCommodity = this.createCommodity.bind(this);
    this.inputName = this.inputName.bind(this);
    this.inputPrice = this.inputPrice.bind(this);
    this.inputUnit = this.inputUnit.bind(this);
    this.inputImageUrl = this.inputImageUrl.bind(this);
  }

  render() {
    return (
      <div className="creator">
        <Nav/>
        <form className="form" onSubmit={this.createCommodity}>
          <label htmlFor="name" className="label">
            <span className="requiredPrompt">*</span>名称：
          </label>
          <input
            id="name"
            className="input"
            placeholder="名称"
            required
            value={this.state.name}
            onChange={this.inputName}
          />

          <label htmlFor="price" className="label">
            <span className="requiredPrompt">*</span>价格：
          </label>
          <input
            id="price"
            className="input"
            placeholder="价格"
            required
            value={this.state.price}
            onChange={this.inputPrice}
          />

          <label htmlFor="unit" className="label">
            <span className="requiredPrompt">*</span>单位：
          </label>
          <input
            id="unit"
            className="input"
            placeholder="单位"
            required
            value={this.state.unit}
            onChange={this.inputUnit}
          />

          <label htmlFor="imageUrl" className="label">
            <span className="requiredPrompt">*</span>图片：
          </label>
          <input
            id="imageUrl"
            className="input"
            placeholder="图片"
            required
            value={this.state.imageUrl}
            onChange={this.inputImageUrl}
          />

          <Button
            onClick={() => {
            }}
            buttonText="提交"
            type="submit"
            color="blue-btn"
            textColor="white-text"
          />
        </form>
      </div>
    );
  }

  createCommodity(event) {
    const headers = {
      'Content-Type': 'application/json;charset=UTF-8'
    };
    const data = {
      name: this.state.name,
      price: this.state.price,
      unit: this.state.unit,
      imageUrl: this.state.imageUrl
    };
    fetchData('/commodities', 'POST', headers, data).then(res => {
      if (res.status === 409) {
        alert("商品名称已存在，请输入新的商品名称");
      } else if (res.status === 400) {
        alert("输入的商品信息有误，请重新输入")
      } else {
        this.props.history.push('/shop');
      }
    });
    event.preventDefault();
  }

  inputName(event) {
    this.setState({
      name: event.target.value
    });
  }

  inputPrice(event) {
    this.setState({
      price: event.target.value
    })
  }

  inputUnit(event) {
    this.setState({
      unit: event.target.value
    })
  }

  inputImageUrl(event) {
    this.setState({
      imageUrl: event.target.value
    })
  }
}

export default Creator;