import { combineReducers } from 'redux';
import orderReducer from './orderReducer';
import shopReducer from './shopReducer';

const reducers = combineReducers({
  order: orderReducer,
  shop: shopReducer
});
export default reducers;
