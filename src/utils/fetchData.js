const url = 'http://localhost:8080/api';
export const fetchData = (api, method = 'GET', headers = {}, body) => {
  return fetch(`${url}${api}`, {
    method: method,
    headers: new Headers(headers),
    body: body ? JSON.stringify(body) : null
  });
};