import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router';
import Shop from "./components/shop/Shop";
import Order from "./components/order/Order";
import Creator from "./components/creator/Creator";
import './App.less';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Shop}/>
            <Route path="/order" component={Order}/>
            <Route path="/create-commodity" component={Creator}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
